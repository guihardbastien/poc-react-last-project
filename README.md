# REACT FRONTEND BOILERPLATE 

## Description

TBA

## Overview

TBA

### Directory structure 

```bash
.
├── App.tsx
├── assets
│   ├── imgs
│   │   └── logo.svg
│   └── styles
│       ├── App.css
│       ├── index.css
│       ├── tailwind.css
│       └── tailwind.output.css
├── components
│   └── DummyComponent
│       └── DummyComponent.tsx
├── config
│   ├── base.ts
│   ├── dev.ts
│   ├── index.ts
│   └── prod.ts
├── i18n
│   ├── en.json
│   ├── fr.json
│   └── index.ts
├── index.tsx
├── pages
│   ├── DummyPage
│   │   └── DummyPage.tsx
│   └── Home
│       └── Home.tsx
├── react-app-env.d.ts
├── tests
│   ├── App.test.tsx
│   └── setupTests.ts
└── types
    └── react-app-env.d.ts

```

### Feature description

TBA

### How to contribute to this project? 

TBA

### How to use?

TBA

## NPM custom commands

- `start`: Start webpack in watch mode for development.
- `build`: Build for production.
- `test`: Run jest tests.
- `format`: Run prettier. 
- `lint`: Run eslint.

## Other/Optional considerations

TBA

## License

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

*Bastien GUIHARD*

