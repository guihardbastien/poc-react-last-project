FROM node:17 AS appbuild
WORKDIR /app
COPY package*.json ./
RUN npm install 
#COPY node_modules ./
COPY . .
CMD ["npm", "run", "start"]
EXPOSE 8080
