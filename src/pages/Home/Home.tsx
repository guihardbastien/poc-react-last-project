import { withTranslation, WithTranslation } from "react-i18next";
import React, { Component } from "react";
import Header from "../../components/Header/Header";
import DataHandler from "../../components/DataHandler/DataHandler";

class Home extends Component<WithTranslation> {
  componentDidMount() {
    return true;
  }

  render() {
    // use for translation
    //const { t } = this.props;
  
    return (
      <div>
        <header className="App-header">
          <Header />
        </header>
        <DataHandler test="style"/>
      </div>
    );
  }
}
export default withTranslation()(Home);
