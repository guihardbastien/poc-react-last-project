import { withTranslation, WithTranslation } from "react-i18next";
import React, { Component } from "react";

interface IProps extends WithTranslation {
  test: String
}

class DataHandler extends Component<IProps> {
  componentDidMount() {
    return true;
  }
  render() {
    // use for translation
    const { t } = this.props;

    return (
      <div>
        <button className="Data-handler-btn">{t("l_importFromServer")}</button>
        <button className="Data-handler-btn">{t("l_localImport")}</button>
        <button className="Data-handler-btn">{t("l_export")}</button>
        <p className="testStyle">{this.props.test}</p>
      </div>
    );
  }
}

export default withTranslation()(DataHandler);
