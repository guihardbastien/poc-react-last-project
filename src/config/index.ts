import _ from "lodash";
import Base from "./base";
import Dev from "./dev";
import Prod from "./prod";

let currentConfig = Dev;

if (process.env.NODE_ENV === "production") {
  currentConfig = Prod;
}

export default _.merge({}, Base, currentConfig);
