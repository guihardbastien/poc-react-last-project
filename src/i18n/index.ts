import { Browser } from "thicker";
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import EN from "./en.json";
import FR from "./fr.json";

const BROWSER_LOCALE = Browser.getDefaultLanguage().toLowerCase();

const resources = {
  en: {
    translation: EN,
  },
  fr: {
    translation: FR,
  },
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: BROWSER_LOCALE,

    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;
