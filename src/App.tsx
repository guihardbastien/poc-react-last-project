import { Switch, Route } from "react-router-dom";
import { useOperationMethod, OpenAPIProvider } from "react-openapi-client";
import React from "react";
import "./assets/styles/App.css";
import "./assets/styles/tailwind.output.css";
import Home from "./pages/Home/Home";
import DummyPage from "./pages/DummyPage/DummyPage";

/*function App(){
  return (
      <OpenAPIProvider definition="http://localhost:3000/openapi.json">
          <Home />
      </OpenAPIProvider>
  )
}
dtest
*/

function App() {
  return (
    <div>
      <Switch>
        <Route path="/dummy">
          <DummyPage />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
